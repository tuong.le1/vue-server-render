import Vue from 'vue'
import Router from 'vue-router'
import VueResource from 'vue-resource'
import About from './components/about.vue'
import Home from './components/home.vue'
import Post from './components/post.vue'

Vue.use(VueResource);
Vue.use(Router)

// const Post = {
// 	template: `
//     <div id="form">
//       <form >
//         <input type="text" placeholder="Work Order Name" v-model="newWorkorder.name">
//         <input type="text" placeholder="Work Order Location" v-model="newWorkorder.location">
//         <input type="text" placeholder="Work Order area" v-model="newWorkorder.area">
//         <input type="text" placeholder="Work Order Area Number" v-model="newWorkorder.areaNumber">
//         <button v-on:click="post">Add Work Order</button>
//       </form>
//     </div>
//   `,
// 	data(){
// 			return{
//         newWorkorder: {
//             name: '',
//             area: '',
//             areaNumber: '',
//             location: '',
//         }
//       }
//   },
//   methods: {
//        post: function() {
//          console.log(123);
//          // this.$http.get("http://localhost:3000/").then(response => {
//          //      console.log(response.body);
//          //    }, response => {
//          //        console.log("error");
//          //    });
//          this.$http.post('http://localhost:3000/post',
//            {
//              name: this.newWorkorder.name
//            }).then((response) => {
//              console.log(response.body);
//            }, (response) => {
//              console.log(response);
//            });
//        }
//   }
// }

export function createRouter () {
  return new Router({
    mode: 'history',
    routes: [
      { path: '/', component: Home },
      { path: '/about', component: About },
      { path: '/post', component: Post }
    ]
  })
}
